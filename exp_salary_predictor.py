from flask import Flask
from flask_restplus import Resource, Api
import joblib
import numpy as np

app = Flask(__name__)
api = Api(app)

model = joblib.load('exp_salary.joblib')


@api.route('/<int:years_exp>')
@api.param('years_exp', 'The number of years experience to get a salary for')
class Salary(Resource):
    def get(self, years_exp):
        salary = model.predict(np.array([years_exp]).reshape(-1, 1))
        return {'years experience received': years_exp,
                'expected salary': salary[0].round(2)}

if __name__ == '__main__':
    app.run(debug=True)
